<?php
define('ROOT', dirname(__FILE__));
include_once ROOT.'/app/GuestBook.php';

if(isset($_GET['page'])){
    $page = (int)$_GET['page'];
    $numberPages = GuestBook::getNumberPages();
    if(!$page){
        $page = 1;
    }
    if($page > $numberPages){
        $page = $numberPages;
        
    }
} else {
    $page = 1;
}

$sort = 'DESC';
$order = 'data_created';

if(isset($_GET['sort'])){
    if($_GET['sort'] == 'ASC'){
        $sort = 'DESC';
    }else{
       $sort = 'ASC';
    }
}

$messageList = GuestBook::getMessageList($page, GuestBook::SHOW_BY_DEFAULT, $sort, $order);
?>        

<table id="tbl-msg" class="table table-striped">
    <thead>
        <tr>
            <th><a class="colomn-sort" id="username" data-page="<?php echo $page; ?>" data-sort="<?php echo $sort; ?>" href="#">Имя</a></th>
            <th><a class="colomn-sort" id="email" data-page="<?php echo $page; ?>" data-sort="<?php echo $sort; ?>" href="#">Email</a></th>
            <th>Сообщение</th>
            <th><a class="colomn-sort" id="data-created" data-page="<?php echo $page; ?>" data-sort="<?php echo $sort; ?>" href="#">Дата</a></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($messageList as $message): ?>
            <tr>
                <td><?php echo $message['username']; ?></td>
                <td><?php echo $message['email']; ?></td>
                <td><?php echo $message['message']; ?></td>
                <td><?php echo $message['data_created']; ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>