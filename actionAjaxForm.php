<?php
define('ROOT', dirname(__FILE__));
include_once ROOT.'/components/recaptcha/recaptchalib.php';
include_once ROOT.'/app/GuestBook.php';

//секретный ключ
$secret = "6LeyIFAUAAAAAFyrfApriR0WiDR0CLsUGB5KvrJ_";
//ответ
$response = null;
//проверка секретного ключа
$reCaptcha = new ReCaptcha($secret);

if ($_SERVER["REQUEST_METHOD"] === 'POST') {
    if (empty($_POST['username'])) {
        $usernameErr = 'Обязательное поле';
    } else {
        //username может содержать только цифры, буквы
        if (GuestBook::checkUsername($_POST['username']) === FALSE) {
            $usernameErr = 'Только цифры и буквы латинского алфавита';
        } else {
            $username = GuestBook::inputData($_POST['username']);
        }
    }

    if (empty($_POST['email'])) {
        $emailErr = 'Обязательное поле';
    } else {
        //Правильно ли сформирован адрес электронной почты
        if (GuestBook::checkEmail($_POST['email']) === FALSE) {
            $emailErr = 'Недопустимый формат электронной почты';
        } else {
            $email = GuestBook::inputData($_POST['email']);
        }
    }

    if (empty($_POST['homepage'])) {
        $homepage = '';
    } else {
        //Проверка корректности синтаксиса URL-адреса
        if (GuestBook::checkHomepage($_POST['homepage']) === FALSE) {
            $homepageErr = 'Некорректный URL';
        } else {
            $homepage = GuestBook::inputData($_POST['homepage']);
        }
    }

    if (empty($_POST['message'])) {
        $messageErr = 'Обязательное поле';
    } else {
        $message = GuestBook::removeHtmlTags($_POST['message']);
    }
    
    if ($_POST['g-recaptcha-response']) {
        $response = $reCaptcha->verifyResponse($_SERVER['REMOTE_ADDR'], $_POST['g-recaptcha-response']);
    }
 
    if ($username != NULL && $email != NULL && $message != NULL && $response != NULL && $response->success && GuestBook::addMessage($username, $email, $homepage, $message)) {
        $ReCaptchaErr = 'Ваше сообщение успешно отправлено!';

        $result = [
            'success' => true,
            'username' => $username,
            'email' => $email,
            'homepage' => $homepage,
            'message' => $message,
            'data_created' => date('Y-m-d H:i:s'),
            'ReCaptchaErr' => $ReCaptchaErr
        ];
    } else {
        $ReCaptchaErr = 'Ошибка! Сообщение не отправлено!';
        
        $result = [
            'success' => false,
            'usernameErr' => $usernameErr,
            'emailErr' => $emailErr,
            'homepageErr' => $homepageErr,
            'messageErr' => $messageErr,
            'ReCaptchaErr' => $ReCaptchaErr
        ];
    }
        
    echo json_encode($result);
} else {
    header('HTTP/1.0 404 Not Found');
}