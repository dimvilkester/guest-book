<?php
/**
 * Description of MyDatabaseException
 *
 * @author DJonny_Di
 */
class MyDatabaseException extends Exception{
  
  public function __construct() {
    parent::__construct("Ошибка в подключении к базе данных");
  }
}
