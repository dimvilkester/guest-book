<?php
include_once ROOT.'/components/MyDatabaseException.php';

Class Db
{
    public static function getConnection() 
    {
        $paramsPath = ROOT.'/config/db_params.php';
        $params = include($paramsPath);
        
        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
        
        try {
            $db = new PDO($dsn, $params['user'], $params['password']);
            $db->exec("set names utf8");
            return $db;
        } catch(PDOException $Exception) {
            echo '<p>';
            echo $Exception->getMessage();
            echo '</p>';
            echo '<p>';
            echo 'File: ' . $Exception->getFile();
            echo '</p>';
            echo '<p>';
            echo 'Error in line: ' . $Exception->getLine();
            echo '</p>';
            throw new MyDatabaseException();
        }       
       
    }

}