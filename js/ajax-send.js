$(document).ready(function(){   
    $('#guestbook-form').on('submit', function () {
        var data = $(this).serialize();   
        $.ajax({
            url: '/actionAjaxForm.php',
            type: 'POST',
            dataType: 'html',
            data: data,
            cache: false,
            success: function (response){
                var result = JSON.parse(response);               
                if(result.success === true){ 
                    $('#tbl-msg').prepend(
                        '<tr>' +
                            '<td>' + result.username + '</td>' +
                            '<td>' + result.email + '</td>' +
                            '<td>' + result.message + '</td>' +
                            '<td>' + result.data_created + '</td>' +
                        '</tr>');                                       
                    $('#recaptcha-err').text(result.ReCaptchaErr);
                    
                    var value = parseInt($('#count-message').text());
                    value = value + 1
                    $('#count-message').text(value);
                } else {
                    $('#username-err').text(result.usernameErr);
                    $('#email-err').text(result.emailErr);
                    $('#homepage-err').text(result.homepageErr);
                    $('#message-err').text(result.messageErr);
                    $('#recaptcha-err').text(result.ReCaptchaErr);
                }
                grecaptcha.reset();
                $('form#guestbook-form')[0].reset();
            },
            error: function (response){
                alert('Error');
                var result = JSON.parse(response);
                if(result.success === false){ 
                    $('#username-err').text(result.usernameErr);
                    $('#email-err').text(result.emailErr);
                    $('#homepage-err').text(result.homepageErr);
                    $('#message-err').text(result.messageErr);
                    $('#recaptcha-err').text(result.ReCaptchaErr);
                }    
                grecaptcha.reset();
                $('form#guestbook-form')[0].reset();
            }
        });
        return false;
    });  
});