$(document).ready(function(){   
    $(document).on('click', '.colomn-sort', function(){
        
        var page = $(this).attr('data-page');
        var order = $(this).attr('id');
        var sort = $(this).attr('data-sort');
        var arrow = '';
        
        if(sort == 'DESC'){
            arrow = '&nbsp;<span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>';
        } else {
            arrow = '&nbsp;<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>';
        }
        
        $.ajax({
            url: '/actionAjaxSort.php',
            type: 'GET',
            data: {page:page, order:order, sort:sort},
            cache: false,
            success: function (response){
                $('#list').html(response);
                $('#'+ order + '').append(arrow);
            },
            error: function (response){
                alert('Error!');
            }
        });
        return false;
    });
});