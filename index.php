<?php
//1. Общие настройки
ini_set('display_errors', 1);
error_reporting(E_ALL);

define('ROOT', dirname(__FILE__));
include_once ROOT.'/app/GuestBook.php';
include_once ROOT.'/components/Pagination.php';
/*************************************************/

if(isset($_GET['page'])){
    $page = (int)$_GET['page'];
    $numberPages = GuestBook::getNumberPages();
    if(!$page){
        $page = 1;
    }
    if($page > $numberPages){
        $page = $numberPages;
        
    }
} else {
    $page = 1;
}

$messageList = GuestBook::getMessageList($page, GuestBook::SHOW_BY_DEFAULT);
$pagination = new Pagination(GuestBook::getCountMessage(), $page, GuestBook::SHOW_BY_DEFAULT, 'page=');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Гостевая книга</title>
        <link rel="stylesheet" href="/css/bootstrap.css" >
        
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <h3>Гостевая книга</h3>
                    
                    <p><b>*</b> <em>Обязательные поля</em></p>
                    
                    <form id="guestbook-form" class="form-horizontal" method="post" action=""> 
                        <div class="form-group">
                            <label for="inputUsername" class="col-sm-2 control-label">Имя: *</label>
                            <div class="col-sm-8">
                                <input type="text" name="username" required="required" class="form-control" id="inputUsername" placeholder="Введите имя">
                                <span id="username-err" class="error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-sm-2 control-label">Email: *</label>
                            <div class="col-sm-8">
                                <input type="email" name="email" required="required" class="form-control" id="inputEmail" placeholder="Введите email">
                                <span id="email-err" class="error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputHomepage" class="col-sm-2 control-label">Homepage: </label>
                            <div class="col-sm-8">
                                <input type="url" name="homepage" class="form-control" id="inputHomepage" placeholder="Введите homepage">
                                <span id="homepage-err" class="error"></span>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label for="inputMessage" class="col-sm-2 control-label">Сообщение: *</label>
                            <div class="col-sm-8">
                                <textarea name="message" required="required" class="form-control" id="inputMessage" rows="4" maxlength="400" placeholder="Введите текст сообщения"></textarea>
                                <span id="message-err" class="error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-8">
                                <div class="g-recaptcha" data-sitekey="6LeyIFAUAAAAAP_qdd0mjeVXZ8OfieNQrOsK60sQ"></div>
                                <span id="recaptcha-err" class="error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-8">
                                <button id="submit" type="submit" name="submit" class="btn btn-default">Отправить</button>
                                <a href="/" name="reset" class="btn btn-danger">Сбросить</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <p><em>Колличество сообщений:</em> <span id="count-message"><?php echo ($countMessage = GuestBook::getCountMessage()) ? $countMessage : $countMessage = 0; ?></span></p>
                    <?php if($messageList): ?>
                        <div id="list">
                            <table id="tbl-msg" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th><a class="colomn-sort" id="username" data-page="<?php echo $page;?>" data-sort="<?php echo $sort;?>" href="#">Имя</a></th>
                                        <th><a class="colomn-sort" id="email" data-page="<?php echo $page;?>" data-sort="<?php echo $sort;?>" href="#">Email</a></th>
                                        <th>Сообщение</th>
                                        <th><a class="colomn-sort" id="data-created" data-page="<?php echo $page;?>" data-sort="<?php echo $sort;?>" href="#">Дата</a></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($messageList as $message): ?>
                                        <tr>
                                            <td><?php echo $message['username']; ?></td>
                                            <td><?php echo $message['email']; ?></td>
                                            <td><?php echo $message['message']; ?></td>
                                            <td><?php echo $message['data_created']; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>    
                    <?php else: ?>
                        <p>Сообщений нет</p>
                    <?php endif; ?>
                </div>
                <div class="col-md-10">
                    <!-- Pagination begin -->
                    <?php echo $pagination->get(); ?>
                    <!-- Pagination end -->
                </div>
            </div>          
            <footer>
                <hr>
                <p>© <?php echo date("Y "); ?> Company, Inc.</p>
            </footer>
        </div>
        
        <script type="text/javascript" src="/js/jquery/jquery.js"></script>
        <script type="text/javascript" src="/js/ajax-send.js"></script>
        <script type="text/javascript" src="/js/ajax-sort.js"></script>
    </body>
</html>