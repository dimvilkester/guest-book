<?php
include_once ROOT.'/components/Db.php';

class GuestBook 
{
    
    const SHOW_BY_DEFAULT = 5;
    
    /**
     * Возвращает число страниц
     * @var int $countPerPage
     * @return int || FALSE
     */
    public static function getNumberPages(int $countPerPage = self::SHOW_BY_DEFAULT) {
        
        $countMessage = self::getCountMessage();
        $countPerPage = intval($countPerPage);
        
        if($countMessage < $countPerPage){
            return FALSE;
        }
        
        $numberPage = intval($countMessage/$countPerPage);
        
        if($countMessage%$countPerPage != 0){
            $numberPage++;
        }
       
        return $numberPage;
    }
    
    /**
     * @var int $page
     * @var int $countPerPage
     * @var int $sort
     * @var int $order
     * @return array || FALSE
     */
    public static function getMessageList(int $page, int $countPerPage = self::SHOW_BY_DEFAULT, string $sort = 'DESC', string $order = 'data_created') {
        
        $countMessage = self::getCountMessage();
        $page = intval(trim($page));
        $countPerPage = intval(trim($countPerPage));
        $order = htmlspecialchars(trim($order));
        $sort = htmlspecialchars(trim($sort));
        
        $start = ($page - 1) * $countPerPage;
        
        $db = Db::getConnection();
        
        $messageList = [];
        
        /* query Выполняет SQL-запрос и возвращает результирующий набор в виде объекта PDOStatement */
        $result = $db->query('SELECT username, email, message, data_created '
                    . 'FROM guestbook_tbl '
                    . 'ORDER BY ' . "$order $sort "
                    . 'LIMIT ' . "$countPerPage"
                    . ' OFFSET ' . "$start");

        /* PDOStatement::fetch — Извлечение следующей строки из результирующего набора */
        $i = 0;
        while ($row = $result->fetch()) {
            $messageList[$i]['username'] = $row['username'];
            $messageList[$i]['email'] = $row['email'];
            $messageList[$i]['message'] = $row['message'];
            $messageList[$i]['data_created'] = $row['data_created'];
            $i++;
        }
        
        if(empty($messageList)){
            return FALSE;
        }    
         
        return $messageList;
    }
    
    /**
     * @var int $username
     * @var int $email
     * @var int $homepage
     * @var int $message
     * @return array || FALSE
     */
    public static function addMessage(string $username, string $email, string $homepage = '', string $message) {
        
        $username = self::inputData($username);
        $email = self::inputData($email);
        $ip = self::getIp();
        $browser = self::getBrowser();
        $message = self::removeHtmlTags($message);
        $homepage = self::inputData($homepage);
        
        $db = Db::getConnection();
        
        $result = $db->prepare('INSERT INTO guestbook_tbl (username, email, ip, browser, homepage, message) '
                . 'VALUES (:username, :email, :ip, :browser, :homepage, :message)');

        return $result->execute(array(
            'username' => $username,
            'email' => $email,
            'ip' => $ip,
            'browser' => $browser,
            'homepage' => $homepage,
            'message' => $message
        ));
    }
    
    /**
     * Возвращает колличество сообщений
     * @return int || FALSE
     */
    public static function getCountMessage() {

        $db = Db::getConnection();

        $result = $db->query('SELECT COUNT(id) FROM guestbook_tbl');
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();
        
        return $count = $row['COUNT(id)'];  
    }
    
    /**
     * @return string
     */
    public static function getIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    
    /**
     * @return string
     */
    public static function getBrowser() {
        if(!empty($_SERVER['HTTP_USER_AGENT'])){
          $user_agent = $_SERVER["HTTP_USER_AGENT"];
        }else{
            return FALSE;   
        } 
        
        if (strpos($user_agent, 'Firefox') !== FALSE){
            $browser = "Firefox";
        }elseif(strpos($user_agent, 'Opera') !== FALSE){
            $browser = "Opera";
        }elseif (strpos($user_agent, 'Chrome') !== FALSE){
            $browser = "Chrome";
        }elseif (strpos($user_agent, 'MSIE') !== FALSE){
            $browser = 'Internet Explorer';
        }elseif (strpos($user_agent, 'Safari') !== FALSE){
            $browser = 'Safari';
        }else{
            $browser = 'Неизвестный';
        }
        
        return $browser;
    }
    
    /**
     * @var mixed $data
     * @return string
     */
    public static function inputData($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    
    /**
     * @var string $username
     * @return string || FALSE
     */
    public static function checkUsername(string $username) {
        $username = self::inputData($username);
        
        if(preg_match("/^[a-zA-Z0-9]*$/", $username)){
           return $username; 
        }
        return FALSE;
    }
    
    /**
     * @var string $email
     * @return string || FALSE
     */
    public static function checkEmail(string $email) {
        $email = self::inputData($email);
        
        if (filter_var($email, FILTER_VALIDATE_EMAIL)){
           return $email; 
        }
        return FALSE;
    }
    
    /**
     * @var string $homepage
     * @return string || FALSE
     */
    public static function checkHomepage(string $homepage) {
        $homepage = self::inputData($homepage);
        
        if (preg_match("/\b(?:(?:https?):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $homepage)) {
           return $homepage; 
        }
        return FALSE;
    }
    
    /**
     * @var string $string
     * @return string
     */
    public static function removeHtmlTags(string $string) {
        return strip_tags(trim($string));
    }
}
